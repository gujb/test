#include "HomePageScene.h" 
#include "HelloWorldScene.h" 
#include "SharedData.h"

using namespace cocos2d;

bool HomePageScene::init(){
	if(CCScene::init()){
        
        //background
        CCSize winsize = CCDirector::sharedDirector()->getWinSize();
        CCSprite* background = CCSprite::create("index_bg.jpg");
        background->setPosition(ccp(winsize.width/2, winsize.height/2));
        this->addChild(background);
        
        //logo
        CCSprite* logo = CCSprite::create("index_logo.png");
        logo->setPosition(ccp(winsize.width/2, winsize.height/2 + 100));
        this->addChild(logo);
        
        //top
        CCSprite* topBar = CCSprite::create("index_top.png");
        topBar->setAnchorPoint(ccp(0,1));
        topBar->setPosition(ccp(0, winsize.height));
        this->addChild(topBar);
        
        //footer
        CCSprite* footBar = CCSprite::create("index_footer.png");
        footBar->setAnchorPoint(ccp(0,0));
        footBar->setPosition(ccp(0, 0));
        this->addChild(footBar);
        
        CCMenuItemImage *moreButton = CCMenuItemImage::create(
            "index_more_button.png","index_more_button.png",this,
            menu_selector(HomePageScene::onMore));
        moreButton->setPosition(ccp(winsize.width/2, 34));
        
		CCMenuItemImage *pPlayButton = CCMenuItemImage::create(
            "play_button.png","play_button.png",this,
            menu_selector(HomePageScene::onPlay));
        pPlayButton->setPosition(ccp(winsize.width/2, winsize.height/2 - 60));
        
        CCMenu* pMenu = CCMenu::create(moreButton, pPlayButton, NULL);
        pMenu->setPosition(ccp(0,0));
        this->addChild(pMenu, 1);
		return true;
	}else{
		return false;
	}
}


void HomePageScene::onPlay(CCObject* pSender){
	CCDirector::sharedDirector()->replaceScene(HelloWorld::scene());
}

void HomePageScene::onMore(CCObject* pSender){
	
}

