#include "HelloWorldScene.h"
#include "SharedData.h"
#include "GameOverScene.h"
#include "GameWinScene.h"
#include "HomePageScene.h"

using namespace cocos2d;

CCScene* HelloWorld::scene()
{
    CCScene * scene = NULL;
    do 
    {
        // 'scene' is an autorelease object
        scene = CCScene::create();
        CC_BREAK_IF(! scene);

        // 'layer' is an autorelease object
		HelloWorld *layer = HelloWorld::create();
        CC_BREAK_IF(!layer);

        // add layer as a child to scene
        scene->addChild(layer);
    } while (0);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    bool bRet = false;
    do 
    {
		if(!CCLayerColor::initWithColor(ccc4(255,255,255,255))){
			return false;
		}
        
        CCSize winsize = CCDirector::sharedDirector()->getWinSize();
        CCSprite* backgroud = CCSprite::create("game_bg.jpg");
        backgroud->setPosition(ccp(winsize.width/2, winsize.height/2));
        this->addChild(backgroud);
		

		CCSprite* top = CCSprite::create("game_top.jpg");
		top->setPosition(ccp(winsize.width/2, winsize.height-top->getContentSize().height/2));
        this->addChild(top);

		CCSprite* foot = CCSprite::create("game_foot_border.png");
		foot->setPosition(ccp(winsize.width/2, foot->getContentSize().height/2));
        this->addChild(foot);

        // 1. Add a menu item with "X" image, which is clicked to quit the program.

        // Create a "close" menu item with close icon, it's an auto release object.
		CCMenuItemImage *pauseBt = CCMenuItemImage::create(
            "pause_button.png",
            "pause_button.png",
            this,
            menu_selector(HelloWorld::menuCloseCallback));
        CC_BREAK_IF(! pauseBt);

        // Place the menu item bottom-right conner.
		 pauseBt->setPosition(ccp(winsize.width - 30, winsize.height-26));

        // Create a menu with the "close" menu item, it's an auto release object.
		 CCMenu* pMenu = CCMenu::create(pauseBt, NULL);
        pMenu->setPosition(CCPointZero);
        CC_BREAK_IF(! pMenu);

        // Add the menu to HelloWorld layer as a child layer.
        this->addChild(pMenu, 1);
       
		BlockNum=0;

		copyData("DataTotal.txt");

		_BlockListClick = new CCArray;
		_BlockList = new CCArray;
		BlockList1 = new CCArray;
		BlockList2 = new CCArray;
		BlockList3 = new CCArray;
		BlockList4 = new CCArray;
		BlockList5 = new CCArray;
		BlockList6 = new CCArray;
		BlockList7 = new CCArray;
		BlockList8 = new CCArray;
		BlockList9 = new CCArray;
		BlockList10 = new CCArray;
		BlockList11 = new CCArray;
		BlockList12 = new CCArray;
		BlockList13 = new CCArray;

		int number=getRandNum();
		BlockList1 = getBlockList(0,number);
		BlockList2 = getBlockList(1,number);
		BlockList3 = getBlockList(2,number);
		BlockList4 = getBlockList(3,number);
		BlockList5 = getBlockList(4,number);
		BlockList6 = getBlockList(5,number);
		BlockList7 = getBlockList(6,number);
		BlockList8 = getBlockList(7,number);
		BlockList9 = getBlockList(8,number);
		BlockList10 = getBlockList(9,number);
		CCSprite* winSprite11 = (CCSprite*)BlockList1->objectAtIndex(1);
		
		_readyBlockList();

		this->setTouchEnabled(true);
		/*
		CCLabelTTF* pLabel2 = CCLabelTTF::create("Level", "Thonburi", 21);
		pLabel2->setPosition(ccp(40,400+pLabel2->getContentSize().height/2));
		pLabel2->setColor(ccc3(255, 255, 255));
		this->addChild(pLabel2, 1);

		CCLabelTTF* pLabel3 = CCLabelTTF::create("Score", "Thonburi", 21);
		pLabel3->setPosition(ccp(135,400+pLabel3->getContentSize().height/2));
		pLabel3->setColor(ccc3(0, 0, 0));
		this->addChild(pLabel3, 1);
		*/
		Singleton* st = Singleton::GetInstance();
		st->setScorePre(st->getScore());
		int level = st->getLevel();
		char level1[8];
		sprintf(level1, "%d", level);

		label2 = CCLabelAtlas::create("12", "fps_images2.png", 15, 21,'.');
		this->addChild(label2);
		label2->setPosition(ccp(60, winsize.height+35));
		label2->setString(level1);

		int score = st->getScore();
		char score1[16];
		sprintf(score1, "%d", score);

		label3 = CCLabelAtlas::create("12", "fps_images2.png", 15, 21,'.');
		this->addChild(label3);
		label3->setPosition(ccp(165, winsize.height+35));
		label3->setString(score1);
		
        bRet = true;
    } while (0);

    return bRet;
}

void HelloWorld::ccTouchesEnded(cocos2d::CCSet* touches, cocos2d::CCEvent* event)
{
	CCTouch* touch = (CCTouch*)(touches->anyObject());
	//CCTouch::getLocationInView()
	CCPoint location = touch->getLocationInView();
	location = CCDirector::sharedDirector()->convertToGL(location);

	float locationX = location.x;
	float locationY = location.y;
	CCSprite* BlockTemp = getOneBlock();
	BlockTemp->setPosition(location);
	CCArray* _runBlockList = new CCArray;
	CCArray* _delBlockList = new CCArray;

	CCSize winsize = CCDirector::sharedDirector()->getWinSize();
	if(locationY-BlockTemp->getContentSize().height*12>0){
		this->removeChild(BlockTemp,true);
		return;
	}
	if(locationX<1 || locationX>=winsize.width-1){
		this->removeChild(BlockTemp,true);
		return;
	}
	CCSprite* winSprite;
	for(int i=0;i<(int)_BlockList->count();i++){
		winSprite = (CCSprite*)_BlockList->objectAtIndex(i);
		if((winSprite->getPosition().y-locationY>=0&&winSprite->getPosition().y-locationY<winSprite->getContentSize().width/2)||
			(locationY-winSprite->getPosition().y>0&&locationY-winSprite->getPosition().y<=winSprite->getContentSize().width/2)){
			if(winSprite->getPosition().x-locationX>=0&&winSprite->getPosition().x-locationX<=winSprite->getContentSize().width/2){
				_runBlockList->addObject(winSprite);
				break;
			}else if(locationX-winSprite->getPosition().x>0&&locationX-winSprite->getPosition().x<winSprite->getContentSize().width/2){
				_runBlockList->addObject(winSprite);
				break;
			}
		}
	}

	this->removeChild(BlockTemp,true);
	if(_runBlockList->count()<1){
		return;
	}
	CCSprite* runBlockListTemp,*BlockListTemp;
	float runBlockListTempX,runBlockListTempY,BlockListTempX,BlockListTempY;

	int numA = 0;
	do{
		numA=_runBlockList->count();
		for(int j=0;j<(int)_BlockList->count();j++){
			BlockListTemp = (CCSprite*)_BlockList->objectAtIndex(j);
			if(_runBlockList->containsObject(BlockListTemp)){
				continue;
			}

			BlockListTempX = BlockListTemp->getPosition().x;
			BlockListTempY = BlockListTemp->getPosition().y;
			int numB=0;
			for(int i=0;i<(int)_runBlockList->count();i++){
				runBlockListTemp = (CCSprite*)_runBlockList->objectAtIndex(i);
				runBlockListTempX = runBlockListTemp->getPosition().x;
				runBlockListTempY = runBlockListTemp->getPosition().y;
			
				float lenth=0;
				if(BlockListTemp->getTag()==runBlockListTemp->getTag()){
					if(runBlockListTempX==BlockListTempX){
						lenth = runBlockListTempY-BlockListTempY;
					}else if(runBlockListTempY==BlockListTempY){
						lenth = runBlockListTempX-BlockListTempX;
					}
					if(lenth<0){
						lenth=0-lenth;
					}
				}
					
				if(lenth>0&&lenth<=runBlockListTemp->getContentSize().width){
					numB=1;
					break;
				}
			}
			if(numB==1){
				_runBlockList->addObject(BlockListTemp);	
			}
		}
	}while((int)_runBlockList->count()>numA);
	
	this->removeChild(label5,true);
	if(_runBlockList->count()>1){
		int NumClick = _runBlockList->count();
		int ClickScore = NumClick*NumClick*5;
		char scoreClick[16];
		sprintf(scoreClick, "%d", ClickScore);

		label5 = CCLabelAtlas::create("12", "fps_images2.png", 15, 21,'.');
		this->addChild(label5);
		label5->setPosition(location);
		label5->setString(scoreClick);
	}

	if(_runBlockList->count()>2)
	{	
		CCSprite* BlockClick;
		if(_BlockListClick->count()>0){
			for(int i=0;i<(int)_runBlockList->count();i++){
				BlockClick=(CCSprite*)_runBlockList->objectAtIndex(i);
				if(!_BlockListClick->containsObject(BlockClick)){
					_BlockListClick->removeAllObjects();
					for(int j=0;j<(int)_runBlockList->count();j++){
						BlockClick=(CCSprite*)_runBlockList->objectAtIndex(j);
						_BlockListClick->addObject(BlockClick);
					}
					return;
				}
			}
		}else{
			for(int i=0;i<(int)_runBlockList->count();i++){
				BlockClick=(CCSprite*)_runBlockList->objectAtIndex(i);
				_BlockListClick->addObject(BlockClick);
			}
			return;
		}
	}
	
	if(_runBlockList->count()>2){
		_BlockListClick->removeAllObjects();
		this->removeChild(label5,true);
		Singleton* st1 = Singleton::GetInstance();
		int score = st1->getScore();
		score=score+5*_runBlockList->count()*_runBlockList->count();
		st1->setScore(score);
		char score1[64]={0};
		sprintf(score1, "%d", st1->getScore());
		label3->setString(score1);
		
		for(int i=0;i<(int)_runBlockList->count();i++){

			if(_BlockList->containsObject(_runBlockList->objectAtIndex(i))){
				_BlockList->removeObject(_runBlockList->objectAtIndex(i));
			}
			this->removeChild((CCSprite*)_runBlockList->objectAtIndex(i),true);
		}
		CCSprite* TempSprite1;
		for(int i=0;i<(int)_runBlockList->count();i++){
			TempSprite1 = (CCSprite*)_runBlockList->objectAtIndex(i);
			if(BlockList1!=NULL&&BlockList1->containsObject(TempSprite1)){
				BlockList1->removeObject(TempSprite1);
				SortBlockList(BlockList1);
				if(BlockList1->count()<1){
					BlockList1=NULL;
				}
				continue;
			}
			if(BlockList2!=NULL&&BlockList2->containsObject(TempSprite1)){
				BlockList2->removeObject(TempSprite1);
				SortBlockList(BlockList2);
				if(BlockList2->count()<1){
					MoveBlockList(BlockList1,1);

					BlockList2=NULL;
				}
				continue;
			}
			if(BlockList3!=NULL&&BlockList3->containsObject(TempSprite1)){
				BlockList3->removeObject(TempSprite1);
				SortBlockList(BlockList3);
				if(BlockList3->count()<1){
					MoveBlockList(BlockList2,1);
					MoveBlockList(BlockList1,1);
						
					BlockList3=NULL;
				}
				continue;
			}
			if(BlockList4!=NULL&&BlockList4->containsObject(TempSprite1)){
				BlockList4->removeObject(TempSprite1);
				SortBlockList(BlockList4);
				if(BlockList4->count()<1){
					MoveBlockList(BlockList3,1);
					MoveBlockList(BlockList2,1);
					MoveBlockList(BlockList1,1);
						
					BlockList4=NULL;
				}
				continue;
			}
			if(BlockList5!=NULL&&BlockList5->containsObject(TempSprite1)){
				BlockList5->removeObject(TempSprite1);
				SortBlockList(BlockList5);
				if(BlockList5->count()<1){
					MoveBlockList(BlockList4,1);
					MoveBlockList(BlockList3,1);
					MoveBlockList(BlockList2,1);
					MoveBlockList(BlockList1,1);
						
					BlockList5=NULL;
				}
				continue;
			}
			if(BlockList6!=NULL&&BlockList6->containsObject(TempSprite1)){
				BlockList6->removeObject(TempSprite1);
				SortBlockList(BlockList6);
				if(BlockList6->count()<1){
					MoveBlockList(BlockList7,-1);
					MoveBlockList(BlockList8,-1);
					MoveBlockList(BlockList9,-1);
					MoveBlockList(BlockList10,-1);
						
					BlockList6=NULL;
				}
				continue;
			}
			if(BlockList7!=NULL){
				if(BlockList7->containsObject(TempSprite1)){
					BlockList7->removeObject(TempSprite1);
					SortBlockList(BlockList7);
					if(BlockList7->count()<1){
						MoveBlockList(BlockList8,-1);
						MoveBlockList(BlockList9,-1);
						MoveBlockList(BlockList10,-1);

						BlockList7=NULL;
					}
					continue;
				}
			}
			if(BlockList8!=NULL&&BlockList8->containsObject(TempSprite1)){
				BlockList8->removeObject(TempSprite1);
				SortBlockList(BlockList8);
				if(BlockList8->count()<1){
					MoveBlockList(BlockList9,-1);
					MoveBlockList(BlockList10,-1);
					MoveBlockList(BlockList11,-1);
					MoveBlockList(BlockList12,-1);
					MoveBlockList(BlockList13,-1);
					
					BlockList8=NULL;
				}
				continue;
			}
			if(BlockList9!=NULL&&BlockList9->containsObject(TempSprite1)){
				BlockList9->removeObject(TempSprite1);
				SortBlockList(BlockList9);
				if(BlockList9->count()<1){
					MoveBlockList(BlockList10,-1);
					MoveBlockList(BlockList11,-1);
					MoveBlockList(BlockList12,-1);
					MoveBlockList(BlockList13,-1);
					
					BlockList9=NULL;
				}
				continue;
			}
			if(BlockList10!=NULL&&BlockList10->containsObject(TempSprite1)){
				BlockList10->removeObject(TempSprite1);
				SortBlockList(BlockList10);
				if(BlockList10->count()<1){
					MoveBlockList(BlockList11,-1);
					MoveBlockList(BlockList12,-1);
					MoveBlockList(BlockList13,-1);
					
					BlockList10=NULL;
				}
				continue;
			}
			if(BlockList11!=NULL&&BlockList11->containsObject(TempSprite1)){
				BlockList11->removeObject(TempSprite1);
				SortBlockList(BlockList11);
				if(BlockList11->count()<1){
					MoveBlockList(BlockList12,-1);
					MoveBlockList(BlockList13,-1);
					
					BlockList11=NULL;
				}
				continue;
			}
			if(BlockList12!=NULL&&BlockList12->containsObject(TempSprite1)){
				BlockList12->removeObject(TempSprite1);
				SortBlockList(BlockList12);
				if(BlockList12->count()<1){
					MoveBlockList(BlockList13,-1);

					BlockList12=NULL;
				}
				continue;
			}
			if(BlockList13!=NULL&&BlockList13->containsObject(TempSprite1)){
				BlockList13->removeObject(TempSprite1);
				SortBlockList(BlockList13);
				if(BlockList13->count()<1){
					BlockList13=NULL;
				}
				continue;
			}
		}
	}
	
	_runBlockList=NULL;
	bool bool1 = spriteFinished();
	
	if(bool1){
		Singleton* st2= Singleton::GetInstance();
		int level = st2->getLevel();
		int DelNum = st2->getDelNum();
		int count=_BlockList->count();
		int nums =count+DelNum-156;

		if(nums<4){
			st2->setStar(5);
		}else if(nums<7){
			st2->setStar(4);
		}else if(nums<10){
			st2->setStar(3);
		}else if(nums<13){
			st2->setStar(2);
		}else if(nums<16){
			st2->setStar(1);
		}else{
			st2->setStar(0);
		}

		if(count<2||(level<16&&count<18-level)){
			level=level+1;
			st2->setLevel(level);
			GameWinScene * GameWinScene = GameWinScene::create();
			GameWinScene->getLayer()->getLabel()->setString("You Win!");
			CCDirector::sharedDirector()->replaceScene(GameWinScene); 
		}else{
			GameOverScene *gameOverScene = GameOverScene::create();
			gameOverScene->getLayer()->getLabel()->setString("You Lose!");
			CCDirector::sharedDirector()->replaceScene(gameOverScene); 
		}
	}
}

void HelloWorld::menuCloseCallback(CCObject* pSender)
{
    // "close" menu item clicked
	CCDirector::sharedDirector()->replaceScene(HomePageScene::create());
}
bool HelloWorld::spriteFinished(){
	CCSprite* tempSprite1,*tempSprite2,*tempSprite3;
	float tempSprite2X,tempSprite2Y,tempSprite3X,tempSprite3Y;
	for(int i=0;i!=_BlockList->count();i++){
		tempSprite1=(CCSprite*)_BlockList->objectAtIndex(i);
		CCArray* tempArr = new CCArray;
		tempArr->addObject(tempSprite1);
		int num=0;
		do{
			num=tempArr->count();
			for(int j=0;j<(int)_BlockList->count();j++){
				tempSprite2=(CCSprite*)_BlockList->objectAtIndex(j);
				if(tempArr->containsObject(tempSprite2)){
					continue;
				}
				tempSprite2X = tempSprite2->getPosition().x;
				tempSprite2Y = tempSprite2->getPosition().y;
				int num1=0;
				for(int l=0;l<(int)tempArr->count();l++){
					tempSprite3=(CCSprite*)tempArr->objectAtIndex(l);
					tempSprite3X = tempSprite3->getPosition().x;
					tempSprite3Y = tempSprite3->getPosition().y;
					float lenth=0;
					if(tempSprite2->getTag()==tempSprite3->getTag()){
						if(tempSprite2X==tempSprite3X){
							lenth = tempSprite3Y-tempSprite2Y;
						}else if(tempSprite3Y==tempSprite2Y){
							lenth = tempSprite3X-tempSprite2X;
						}
						if(lenth<0){
							lenth=0-lenth;
						}
						if(lenth>0&&lenth<=tempSprite3->getContentSize().width){
							num1=1;
							break;
						}
					}
				}
				if(num1==1){
					tempArr->addObject(tempSprite2);	
				}
			}
			if(tempArr->count()>2){
				return false;
			}
		}while(((int)tempArr->count())>num);
		
		tempArr=NULL;
	}
	return true;
}

//initialize _BlockList
void HelloWorld::_readyBlockList()
{
	CCSprite* TempBlock;
	for(int i=0;i<(int)BlockList1->count();i++){
		TempBlock=(CCSprite*)BlockList1->objectAtIndex(i);
		_BlockList->addObject(TempBlock);
	}
	for(int i=0;i<(int)BlockList2->count();i++){
		TempBlock=(CCSprite*)BlockList2->objectAtIndex(i);
		_BlockList->addObject(TempBlock);
	}
	for(int i=0;i<(int)BlockList3->count();i++){
		TempBlock=(CCSprite*)BlockList3->objectAtIndex(i);
		_BlockList->addObject(TempBlock);
	}
	for(int i=0;i<(int)BlockList4->count();i++){
		TempBlock=(CCSprite*)BlockList4->objectAtIndex(i);
		_BlockList->addObject(TempBlock);
	}
	for(int i=0;i<(int)BlockList5->count();i++){
		TempBlock=(CCSprite*)BlockList5->objectAtIndex(i);
		_BlockList->addObject(TempBlock);
	}
	for(int i=0;i<(int)BlockList6->count();i++){
		TempBlock=(CCSprite*)BlockList6->objectAtIndex(i);
		_BlockList->addObject(TempBlock);
	}
	for(int i=0;i<(int)BlockList7->count();i++){
		TempBlock=(CCSprite*)BlockList7->objectAtIndex(i);
		_BlockList->addObject(TempBlock);
	}
	for(int i=0;i<(int)BlockList8->count();i++){
		TempBlock=(CCSprite*)BlockList8->objectAtIndex(i);
		_BlockList->addObject(TempBlock);
	}
	for(int i=0;i<(int)BlockList9->count();i++){
		TempBlock=(CCSprite*)BlockList9->objectAtIndex(i);
		_BlockList->addObject(TempBlock);
	}
	for(int i=0;i<(int)BlockList10->count();i++){
		TempBlock=(CCSprite*)BlockList10->objectAtIndex(i);
		_BlockList->addObject(TempBlock);
	}
}
void HelloWorld::copyData(const char* pFileName){
	std::string strPath = CCFileUtils::sharedFileUtils()->fullPathFromRelativePath(pFileName);
	//string strPath = CCFileUtils::fullPathFromRelativePath(pFileName);
	unsigned long len = 0;
	unsigned char *data = NULL;
	data = CCFileUtils::sharedFileUtils()->getFileData(strPath.c_str(),"r",&len);
	std::string destPath = CCFileUtils::sharedFileUtils()->getWriteablePath();
	destPath += pFileName;
	FILE *fp = fopen(destPath.c_str(),"w+");
	fwrite(data,sizeof(char),len,fp);
	fclose(fp);
	delete []data;
	data = NULL;
}
//set a list's Tags
void HelloWorld::SetBlockTag(CCArray* BlockList,int level){
	const char *filepath = CCFileUtils::sharedFileUtils()->fullPathFromRelativePath("DataTotal.txt");
	char buf[160];
	FILE* m_pFile = fopen(filepath,"r");
	int i=1;
	while(fgets(buf,sizeof(buf),m_pFile)){
		if(level==i){
			break;
		}
		i=i+1;
	}
	CCSprite* Block;
	int j=0;
	for(int i=0;i<(int)BlockList->count();i++){
		Block=(CCSprite*)BlockList->objectAtIndex(i);
		Block->setTag(buf[j]);
		j=j+1;
	}
}

//print a list's Tags
void HelloWorld::PrintBlockTag(CCArray* BlockList){
	CCSprite* Block;
	int i=0;
	char BlockTagArr[158]={0};
	for(int i=0;i<(int)BlockList->count();i++){
		Block=(CCSprite*)BlockList->objectAtIndex(i);
		BlockTagArr[i] = Block->getTag()+'0';
		i=i+1;
	}
	BlockTagArr[156]=10;

	const char *pFileName="DataTotal.txt";
	std::string destPath = CCFileUtils::sharedFileUtils()->getWriteablePath();
	destPath += pFileName;
	unsigned long len = 0;
	unsigned char *data = NULL;
	data = (unsigned char *)BlockTagArr;
	len = strlen((const char *)data);
	
	FILE *fp = fopen(destPath.c_str(),"a+");
	fwrite(data,sizeof(char),len,fp);    
	fclose(fp);
	
}
//Get a list of Block
CCArray* HelloWorld::getBlockList(int n,int row)
{
	CCSprite* Block,* BlockTemp;
	CCArray* BlockList = new CCArray;
	const char *filepath = CCFileUtils::sharedFileUtils()->fullPathFromRelativePath("DataTotal.txt");
	char buf[170]={0};
	for(int i=0;i<170;i++){
		buf[i]=49;
	}
	std::string strFilePath = CCFileUtils::sharedFileUtils()->getWriteablePath();
	strFilePath = strFilePath+"DataTotal.txt";
	//CCLog(strFilePath.c_str());
	FILE* m_pFile = fopen(strFilePath.c_str(),"r");
	if(m_pFile){
		int k=1;
		char buf1[170]={0};
		fgets(buf,sizeof(buf),m_pFile);
		while(k<row){
			fgets(buf,sizeof(buf),m_pFile);
			k=k+1;
		}
		fclose(m_pFile);
	}

	Singleton* st= Singleton::GetInstance();
	int DelNums=(buf[157]-48)*100+(buf[158]-48)*10+(buf[159]-48);
	st->setDelNum(DelNums);

	int num=0;
	for(int i=0;i<12;i++)
	{
		num=n*12+i;
		if(i>0){
			for(int it=0;it<(int)BlockList->count();it++){
				BlockTemp = (CCSprite*)BlockList->objectAtIndex(it);
				BlockTemp->setPosition(ccp(BlockTemp->getPosition().x,BlockTemp->getPosition().y+BlockTemp->getContentSize().height));
			}
		}
		
		Block = getOneBlock(buf[num]);
		Block->setPosition(ccp(Block->getContentSize().width*n+Block->getContentSize().width/2,Block->getContentSize().height/2+24));
		BlockList->addObject(Block);
	}
	
	return BlockList;
}

//Get a Block
CCSprite* HelloWorld::getOneBlock(int num)
{	
	CCSprite* Block;
	num=num-48;
	switch(num)
	{
		case 1:
			Block=CCSprite::create("DiamondBlue.png",CCRectMake(0,0,32,32));
			Block->setTag(1);
			break;
		case 2:
			Block=CCSprite::create("DiamondGreen.png",CCRectMake(0,0,32,32));
			Block->setTag(2);
			break;
		case 3:
			Block=CCSprite::create("DiamondPurple.png",CCRectMake(0,0,32,32));
			Block->setTag(3);
			break;
		case 4:
			Block=CCSprite::create("DiamondRed.png",CCRectMake(0,0,32,32));
			Block->setTag(4);
			break;
		case 5:
			Block=CCSprite::create("DiamondRoseo.png",CCRectMake(0,0,32,32));
			Block->setTag(5);
			break;
		case 6:
			Block=CCSprite::create("DiamondYellow.png",CCRectMake(0,0,32,32));
			Block->setTag(6);
			break;
	}
	
	this->addChild(Block);
	return Block;
}

//Get a list of Block
CCArray* HelloWorld::getBlockList(int n)
{
	CCSprite* Block,* BlockTemp;
	CCArray* BlockList = new CCArray;
	
	for(int i=0;i<12;i++)
	{
		if(i>0){
			for(int it=0;it<(int)BlockList->count();it++){
				BlockTemp = (CCSprite*)BlockList->objectAtIndex(it);
				BlockTemp->setPosition(ccp(BlockTemp->getPosition().x,BlockTemp->getPosition().y+BlockTemp->getContentSize().height));
			}
		}
		Block = getOneBlock();
		Block->setPosition(ccp(Block->getContentSize().width*n+Block->getContentSize().width/2,Block->getContentSize().height));
		BlockList->addObject(Block);
	}
	
	return BlockList;
}

//Get a Block
CCSprite* HelloWorld::getOneBlock()
{	
	//srand(time(NULL)+rand());
	//BlockNum=BlockNum+time(NULL)+rand();
	//srand(BlockNum+BlockNum%11+(BlockNum%3*BlockNum%7));
	int num1 = rand()%3+1;
	CCSprite* Block;
	switch(num1)
	{
		case 1:
			Block=CCSprite::create("DiamondBlue.png",CCRectMake(0,0,32,32));
			Block->setTag(1);
			break;
		case 2:
			Block=CCSprite::create("DiamondGreen.png",CCRectMake(0,0,32,32));
			Block->setTag(2);
			break;
		case 3:
			Block=CCSprite::create("DiamondPurple.png",CCRectMake(0,0,32,32));
			Block->setTag(3);
			break;
		case 4:
			Block=CCSprite::create("DiamondRed.png",CCRectMake(0,0,32,32));
			Block->setTag(4);
			break;
		case 5:
			Block=CCSprite::create("DiamondRoseo.png",CCRectMake(0,0,32,32));
			Block->setTag(5);
			break;
		case 6:
			Block=CCSprite::create("DiamondYellow.png",CCRectMake(0,0,32,32));
			Block->setTag(6);
			break;
	}
	
	this->addChild(Block);
	return Block;
}
int HelloWorld::getRandNum(){
	Singleton* st = Singleton::GetInstance();
	int level=st->getLevel();

	srand(time(NULL)+rand());
	BlockNum=BlockNum+time(NULL)+rand();
	srand(BlockNum+BlockNum%21+BlockNum%11);
	int num1 = rand()%21+1;
	return num1;
}

//Move the BlockList to the left or right
void HelloWorld::MoveBlockList(CCArray* BlockList,int Direction){
	CCSprite* ListTemp;
	if(BlockList!=NULL){
		for(int it=0;it<(int)BlockList->count();it++){
			ListTemp=(CCSprite*)BlockList->objectAtIndex(it);
			ListTemp->setPosition(ccp(ListTemp->getPosition().x+Direction*ListTemp->getContentSize().width,ListTemp->getPosition().y));
		}
	}
}

//Reset position in the BlockList of elements
void HelloWorld::SortBlockList(cocos2d::CCArray* BlockList){
	int listNum=BlockList->count();
	CCSprite* ListTemp;;
	for(int it=0;it<(int)BlockList->count();it++){
		ListTemp=(CCSprite*)BlockList->objectAtIndex(it);
		listNum--;
		ListTemp->setPosition(ccp(ListTemp->getPosition().x,ListTemp->getContentSize().height*listNum+ListTemp->getContentSize().height/2+24));
	}
}

int HelloWorld::countLeftBlock(){
	int it,jt,kt,lt,mt,nt,xt,yt;
	//记录13个list
	cocos2d::CCArray* BlockList1_1=new CCArray;
	cocos2d::CCArray* BlockList2_1=new CCArray;
	cocos2d::CCArray* BlockList3_1=new CCArray;
	cocos2d::CCArray* BlockList4_1=new CCArray;
	cocos2d::CCArray* BlockList5_1=new CCArray;
	cocos2d::CCArray* BlockList6_1=new CCArray;
	cocos2d::CCArray* BlockList7_1=new CCArray;
	cocos2d::CCArray* BlockList8_1=new CCArray;
	cocos2d::CCArray* BlockList9_1=new CCArray;
	cocos2d::CCArray* BlockList10_1=new CCArray;
	cocos2d::CCArray* BlockList11_1=new CCArray;
	cocos2d::CCArray* BlockList12_1=new CCArray;
	cocos2d::CCArray* BlockList13_1=new CCArray;

	//BlockList0记录所有原始的Bloak
	cocos2d::CCArray* BlockList0=new CCArray;
	//BlockList0_1 存储本次全部的Block;
	cocos2d::CCArray* BlockList0_1=new CCArray;
	//BlockList0_2 存储本次已判断的Block;
	cocos2d::CCArray* BlockList0_2=new CCArray;
	//BlockList0_3 存储即将判断的Block;
	cocos2d::CCArray* BlockList0_3=new CCArray;
	//BlockList0_4 计算是否结束本次计算
	cocos2d::CCArray* BlockList0_4=new CCArray;
	//记录已判断过的Block
	cocos2d::CCArray* BlockList0_5=new CCArray;
	//记录要删除的Block
	cocos2d::CCArray* BlockList0_6=new CCArray;
	//临时记录要删除的BlockList0中的Block
	cocos2d::CCArray* BlockList0_7=new CCArray;

	CCSprite* ListTemp,* ListTemp1;
	int countBlock=0;
	int countBlockTemp=0;
	int BlockNums=0;
	int BlockNumTemp=0;

	//删除的Block数量
	int LeftNum=0;
	//本局得分
	int BlockScore=0;

	//给BlockList0赋值
	for(it=0;it<(int)_BlockList->count();it++){
		ListTemp=(CCSprite*)_BlockList->objectAtIndex(it);
		ListTemp1 =new CCSprite();
		ListTemp1->setPosition(ListTemp->getPosition());
		ListTemp1->setTag(ListTemp->getTag());
		ListTemp1->setContentSize(ListTemp->getContentSize());

		BlockList0->addObject(ListTemp1);
	}

	int bools=0;
	do{
		int boolNum=0;
		int bools_1=0;
		BlockList0_5->removeAllObjects();
		do{
			BlockList0_1->removeAllObjects();
			BlockList0_2->removeAllObjects();
			BlockList0_3->removeAllObjects();
			BlockList0_4->removeAllObjects();

			BlockList1_1->removeAllObjects();
			BlockList2_1->removeAllObjects();
			BlockList3_1->removeAllObjects();
			BlockList4_1->removeAllObjects();
			BlockList5_1->removeAllObjects();
			BlockList6_1->removeAllObjects();
			BlockList7_1->removeAllObjects();
			BlockList8_1->removeAllObjects();
			BlockList9_1->removeAllObjects();
			BlockList10_1->removeAllObjects();
			BlockList11_1->removeAllObjects();
			BlockList12_1->removeAllObjects();
			BlockList13_1->removeAllObjects();

			for(xt=0;xt<(int)BlockList0->count();xt++){
				ListTemp=(CCSprite*)BlockList0->objectAtIndex(xt);
				ListTemp1 =new CCSprite();
				ListTemp1->setPosition(ListTemp->getPosition());
				ListTemp1->setTag(ListTemp->getTag());
				ListTemp1->setContentSize(ListTemp->getContentSize());

				if(ListTemp1->getPosition().x<ListTemp1->getContentSize().width){
					BlockList1_1->addObject(ListTemp1);
				}else if(ListTemp1->getPosition().x<ListTemp1->getContentSize().width*2){
					BlockList2_1->addObject(ListTemp1);
				}else if(ListTemp1->getPosition().x<ListTemp1->getContentSize().width*3){
					BlockList3_1->addObject(ListTemp1);
				}else if(ListTemp1->getPosition().x<ListTemp1->getContentSize().width*4){
					BlockList4_1->addObject(ListTemp1);
				}else if(ListTemp1->getPosition().x<ListTemp1->getContentSize().width*5){
					BlockList5_1->addObject(ListTemp1);
				}else if(ListTemp1->getPosition().x<ListTemp1->getContentSize().width*6){
					BlockList6_1->addObject(ListTemp1);
				}else if(ListTemp1->getPosition().x<ListTemp1->getContentSize().width*7){
					BlockList7_1->addObject(ListTemp1);
				}else if(ListTemp1->getPosition().x<ListTemp1->getContentSize().width*8){
					BlockList8_1->addObject(ListTemp1);
				}else if(ListTemp1->getPosition().x<ListTemp1->getContentSize().width*9){
					BlockList9_1->addObject(ListTemp1);
				}else if(ListTemp1->getPosition().x<ListTemp1->getContentSize().width*10){
					BlockList10_1->addObject(ListTemp1);
				}else if(ListTemp1->getPosition().x<ListTemp1->getContentSize().width*11){
					BlockList11_1->addObject(ListTemp1);
				}else if(ListTemp1->getPosition().x<ListTemp1->getContentSize().width*12){
					BlockList12_1->addObject(ListTemp1);
				}else if(ListTemp1->getPosition().x<ListTemp1->getContentSize().width*13){
					BlockList13_1->addObject(ListTemp1);
				}
				
				BlockList0_1->addObject(ListTemp1);
			}
			//整理13个数组
			if(BlockList1_1->count()>0){
				SortBlockList(BlockList1_1);
			}
			if(BlockList2_1->count()>0){
				SortBlockList(BlockList2_1);
			}else{
				MoveBlockList(BlockList1_1,1);
			}
			if(BlockList3_1->count()>0){
				SortBlockList(BlockList3_1);
			}else{
				MoveBlockList(BlockList2_1,1);
				MoveBlockList(BlockList1_1,1);
			}
			if(BlockList4_1->count()>0){
				SortBlockList(BlockList4_1);
			}else{
				MoveBlockList(BlockList3_1,1);
				MoveBlockList(BlockList2_1,1);
				MoveBlockList(BlockList1_1,1);
			}
			if(BlockList5_1->count()>0){
				SortBlockList(BlockList5_1);
			}else{
				MoveBlockList(BlockList4_1,1);
				MoveBlockList(BlockList3_1,1);
				MoveBlockList(BlockList2_1,1);
				MoveBlockList(BlockList1_1,1);
			}
			if(BlockList6_1->count()>0){
				SortBlockList(BlockList6_1);
			}else{
				MoveBlockList(BlockList5_1,1);
				MoveBlockList(BlockList4_1,1);
				MoveBlockList(BlockList3_1,1);
				MoveBlockList(BlockList2_1,1);
				MoveBlockList(BlockList1_1,1);
			}
			if(BlockList7_1->count()>0){
				SortBlockList(BlockList7_1);
			}else{
				MoveBlockList(BlockList6_1,1);
				MoveBlockList(BlockList5_1,1);
				MoveBlockList(BlockList4_1,1);
				MoveBlockList(BlockList3_1,1);
				MoveBlockList(BlockList2_1,1);
				MoveBlockList(BlockList1_1,1);
			}
			if(BlockList8_1->count()>0){
				SortBlockList(BlockList8_1);
			}else{
				MoveBlockList(BlockList9_1,-1);
				MoveBlockList(BlockList10_1,-1);
				MoveBlockList(BlockList11_1,-1);
				MoveBlockList(BlockList12_1,-1);
				MoveBlockList(BlockList13_1,-1);
			}
			if(BlockList9_1->count()>0){
				SortBlockList(BlockList9_1);
			}else{
				MoveBlockList(BlockList10_1,-1);
				MoveBlockList(BlockList11_1,-1);
				MoveBlockList(BlockList12_1,-1);
				MoveBlockList(BlockList13_1,-1);
			}
			if(BlockList10_1->count()>0){
				SortBlockList(BlockList10_1);
			}else{
				MoveBlockList(BlockList11_1,-1);
				MoveBlockList(BlockList12_1,-1);
				MoveBlockList(BlockList13_1,-1);
			}
			if(BlockList11_1->count()>0){
				SortBlockList(BlockList11_1);
			}else{
				MoveBlockList(BlockList12_1,-1);
				MoveBlockList(BlockList13_1,-1);
			}
			if(BlockList12_1->count()>0){
				SortBlockList(BlockList12_1);
			}else{
				MoveBlockList(BlockList13_1,-1);
			}
			if(BlockList13_1->count()>0){
				SortBlockList(BlockList13_1);
			}

			//判定是否结束；
			CCSprite* tempSprite1,*tempSprite2,*tempSprite3;
			float tempSprite2X,tempSprite2Y,tempSprite3X,tempSprite3Y;
			int num=0;
			int num1=0;
			float lenth1=0;
			bools_1=1;
			for(it=0;it<(int)BlockList0_1->count();it++){
				BlockList0_4->removeAllObjects();
				tempSprite1=(CCSprite*)BlockList0_1->objectAtIndex(it);
				if(BlockList0_3->containsObject(tempSprite1)){
					continue;
				}
				boolNum=0;
				for(yt=0;yt<(int)BlockList0_5->count();yt++){
					ListTemp1=(CCSprite*)BlockList0_5->objectAtIndex(yt);
					if(tempSprite1->getTag()==ListTemp1->getTag()){
						if(tempSprite1->getPosition().x==ListTemp1->getPosition().x&&tempSprite1->getPosition().y==ListTemp1->getPosition().y){
							boolNum=1;
						}
					}
					if(boolNum==1){
						break;
					}
				}
				if(boolNum==1){
					continue;
				}

				BlockList0_4->addObject(tempSprite1);
				num=0;
				do{
					num=BlockList0_4->count();
					for(jt=0;jt<(int)BlockList0_1->count();jt++){
						tempSprite2=(CCSprite*)BlockList0_1->objectAtIndex(jt);
						if(BlockList0_4->containsObject(tempSprite2)){
							continue;
						}
						if(BlockList0_3->containsObject(tempSprite2)){
							continue;
						}
						boolNum=0;
						for(yt=0;yt<(int)BlockList0_5->count();yt++){
							ListTemp1=(CCSprite*)BlockList0_5->objectAtIndex(yt);
							if(tempSprite2->getTag()==ListTemp1->getTag()){
								if(tempSprite2->getPosition().x==ListTemp1->getPosition().x&&tempSprite2->getPosition().y==ListTemp1->getPosition().y){
									boolNum=1;
								}
							}
							if(boolNum==1){
								break;
							}
						}
						if(boolNum==1){
							continue;
						}
						tempSprite2X = tempSprite2->getPosition().x;
						tempSprite2Y = tempSprite2->getPosition().y;
						num1=0;
						for(kt=0;kt<(int)BlockList0_4->count();kt++){
							tempSprite3=(CCSprite*)BlockList0_4->objectAtIndex(kt);
							tempSprite3X = tempSprite3->getPosition().x;
							tempSprite3Y = tempSprite3->getPosition().y;
							lenth1=0;
							if(tempSprite2->getTag()==tempSprite3->getTag()){
								if(tempSprite2X==tempSprite3X){
									lenth1 = tempSprite3Y-tempSprite2Y;
								}else if(tempSprite3Y==tempSprite2Y){
									lenth1 = tempSprite3X-tempSprite2X;
								}
								if(lenth1<0){
									lenth1=0-lenth1;
								}
								if(lenth1>0&&lenth1<=tempSprite3->getContentSize().width){
									num1=1;
									break;
								}
							}
						}
						if(num1==1){
							BlockList0_4->addObject(tempSprite2);	
						}
					}
				}while((int)BlockList0_4->count()>num);
				
				if(BlockList0_4->count()<3){
					bools_1=1;
				}else{
					bools_1=0;
					break;
				}
				for(kt=0;kt<(int)BlockList0_4->count();kt++){
					tempSprite1=(CCSprite*)BlockList0_4->objectAtIndex(kt);
					BlockList0_3->addObject(tempSprite1);
				}
			}

			BlockList0_3->removeAllObjects();
			BlockList0_4->removeAllObjects();
			if(bools_1==0){
				for(jt=0;jt<(int)BlockList0_1->count();jt++){
					BlockList0_3->removeAllObjects();
					ListTemp=(CCSprite*)BlockList0_1->objectAtIndex(jt);
					if(BlockList0_2->containsObject(ListTemp)){
						continue;
					}
					BlockList0_3->addObject(ListTemp);

					//判断连续的Block,存储在BlockList0_3中
					CCSprite *BlockTemp1,*BlockTemp2;
					float BlockTemp1X,BlockTemp1Y,BlockTemp2X,BlockTemp2Y;
					int numA=0;
					int numB=0;
					float lenth=0;
					do{
						numA=BlockList0_3->count();
						for(kt=0;kt<(int)BlockList0_1->count();kt++){
							BlockTemp1=(CCSprite*)BlockList0_1->objectAtIndex(kt);
							if(BlockList0_3->containsObject(BlockTemp1)){
								continue;
							}
							if(BlockList0_2->containsObject(BlockTemp1)){
								continue;
							}
							if(countBlockTemp==0){
								boolNum=0;
								for(yt=0;yt<(int)BlockList0_5->count();yt++){
									ListTemp1=(CCSprite*)BlockList0_5->objectAtIndex(yt);
									if(BlockTemp1->getTag()==ListTemp1->getTag()){
										if(BlockTemp1->getPosition().x==ListTemp1->getPosition().x&&BlockTemp1->getPosition().y==ListTemp1->getPosition().y){
											boolNum=1;
										}
									}
									if(boolNum==1){
										break;
									}
								}
								if(boolNum==1){
									continue;
								}
							}
							BlockTemp1X = BlockTemp1->getPosition().x;
							BlockTemp1Y = BlockTemp1->getPosition().y;
							numB=0;
							for(lt=0;lt<(int)BlockList0_3->count();lt++){
								BlockTemp2=(CCSprite*)BlockList0_3->objectAtIndex(lt);
								BlockTemp2X = BlockTemp2->getPosition().x;
								BlockTemp2Y = BlockTemp2->getPosition().y;
								lenth=0;
								if(BlockTemp1->getTag()==BlockTemp2->getTag()){
									if(BlockTemp1X==BlockTemp2X){
										lenth = BlockTemp1Y-BlockTemp2Y;
									}else if(BlockTemp1Y==BlockTemp2Y){
										lenth = BlockTemp1X-BlockTemp2X;
									}
									if(lenth<0){
										lenth=0-lenth;
									}
								}
								if(lenth==BlockTemp1->getContentSize().width){
									numB=1;
									break;
								}
							}
							if(numB==1){
								BlockList0_3->addObject(BlockTemp1);	
							}
						}
					}while((int)BlockList0_3->count()>numA);
					
					if(countBlockTemp>0){
						if((int)BlockList0_3->count()>2){
							BlockNumTemp = BlockNumTemp+((int)BlockList0_3->count());
							countBlockTemp=countBlockTemp+((int)BlockList0_3->count())*((int)BlockList0_3->count())*5;
						}
						for(it=0;it<(int)BlockList0_3->count();it++){
							BlockList0_2->addObject((CCSprite*)BlockList0_3->objectAtIndex(it));
						}
					}else if((int)BlockList0_3->count()>2){
						BlockNumTemp =BlockNumTemp +((int)BlockList0_3->count());
						countBlockTemp=((int)BlockList0_3->count())*((int)BlockList0_3->count())*5;
						CCSprite *BlockTemp3,*BlockTemp4,*BlockTemp5;
						for(mt=0;mt<(int)BlockList0_3->count();mt++){
							BlockTemp3=(CCSprite*)BlockList0_3->objectAtIndex(mt);
							BlockList0_2->addObject(BlockTemp3);
							BlockTemp4=new CCSprite();
							BlockTemp4->setTag(BlockTemp3->getTag());
							BlockTemp4->setPosition(BlockTemp3->getPosition());
							BlockTemp4->setContentSize(BlockTemp3->getContentSize());
							BlockList0_4->addObject(BlockTemp4);

						}
						for(mt=0;mt<(int)BlockList0_3->count();mt++){
							BlockTemp5=(CCSprite*)BlockList0_3->objectAtIndex(mt);
							if((int)BlockList1_1->count()>0&&BlockList1_1->containsObject(BlockTemp5)){
								BlockList1_1->removeObject(BlockTemp5);
								SortBlockList(BlockList1_1);
								continue;
							}
							if((int)BlockList2_1->count()>0&&BlockList2_1->containsObject(BlockTemp5)){
								BlockList2_1->removeObject(BlockTemp5);
								SortBlockList(BlockList2_1);
								if(BlockList2_1->count()<1){
									MoveBlockList(BlockList1_1,1);
								}
								continue;
							}
							if((int)BlockList3_1->count()>0&&BlockList3_1->containsObject(BlockTemp5)){
								BlockList3_1->removeObject(BlockTemp5);
								SortBlockList(BlockList3_1);
								if(BlockList3_1->count()<1){
									MoveBlockList(BlockList2_1,1);
									MoveBlockList(BlockList1_1,1);
								}
								continue;
							}
							if((int)BlockList4_1->count()>0&&BlockList4_1->containsObject(BlockTemp5)){
								BlockList4_1->removeObject(BlockTemp5);
								SortBlockList(BlockList4_1);
								if(BlockList4_1->count()<1){
									MoveBlockList(BlockList3_1,1);
									MoveBlockList(BlockList2_1,1);
									MoveBlockList(BlockList1_1,1);
								}
								continue;
							}
							if((int)BlockList5_1->count()>0&&BlockList5_1->containsObject(BlockTemp5)){
								BlockList5_1->removeObject(BlockTemp5);
								SortBlockList(BlockList5_1);
								if(BlockList5_1->count()<1){
									MoveBlockList(BlockList4_1,1);
									MoveBlockList(BlockList3_1,1);
									MoveBlockList(BlockList2_1,1);
									MoveBlockList(BlockList1_1,1);
								}
								continue;
							}
							if((int)BlockList6_1->count()>0&&BlockList6_1->containsObject(BlockTemp5)){
								BlockList6_1->removeObject(BlockTemp5);
								SortBlockList(BlockList6_1);
								if(BlockList6_1->count()<1){
									MoveBlockList(BlockList5_1,1);
									MoveBlockList(BlockList4_1,1);
									MoveBlockList(BlockList3_1,1);
									MoveBlockList(BlockList2_1,1);
									MoveBlockList(BlockList1_1,1);
								}
								continue;
							}
							if((int)BlockList7_1->count()>0&&BlockList7_1->containsObject(BlockTemp5)){
								BlockList7_1->removeObject(BlockTemp5);
								SortBlockList(BlockList7_1);
								if(BlockList7_1->count()<1){
									MoveBlockList(BlockList6_1,1);
									MoveBlockList(BlockList5_1,1);
									MoveBlockList(BlockList4_1,1);
									MoveBlockList(BlockList3_1,1);
									MoveBlockList(BlockList2_1,1);
									MoveBlockList(BlockList1_1,1);
								}
								continue;
							}
							if((int)BlockList8_1->count()>0&&BlockList8_1->containsObject(BlockTemp5)){
								BlockList8_1->removeObject(BlockTemp5);
								SortBlockList(BlockList8_1);
								if(BlockList8_1->count()<1){
									MoveBlockList(BlockList9_1,-1);
									MoveBlockList(BlockList10_1,-1);
									MoveBlockList(BlockList11_1,-1);
									MoveBlockList(BlockList12_1,-1);
									MoveBlockList(BlockList13_1,-1);
								}
								continue;
							}
							if((int)BlockList9_1->count()>0&&BlockList9_1->containsObject(BlockTemp5)){
								BlockList9_1->removeObject(BlockTemp5);
								SortBlockList(BlockList9_1);
								if(BlockList9_1->count()<1){
									MoveBlockList(BlockList10_1,-1);
									MoveBlockList(BlockList11_1,-1);
									MoveBlockList(BlockList12_1,-1);
									MoveBlockList(BlockList13_1,-1);
								}
								continue;
							}
							if((int)BlockList10_1->count()>0&&BlockList10_1->containsObject(BlockTemp5)){
								BlockList10_1->removeObject(BlockTemp5);
								SortBlockList(BlockList10_1);
								if(BlockList10_1->count()<1){
									MoveBlockList(BlockList11_1,-1);
									MoveBlockList(BlockList12_1,-1);
									MoveBlockList(BlockList13_1,-1);
								}
								continue;
							}
						}
					}
				}
				
				/*
				if(countBlockTemp>countBlock){
					countBlock=countBlockTemp;
					BlockList0_6->removeAllObjects();
					for(nt=BlockList0_4->begin();nt!=BlockList0_4->end();nt++){
						BlockList0_6->addObject(*nt);
					}
				}
				//*/
				///*
				if(BlockNumTemp>BlockNums){
					BlockNums=BlockNumTemp;
					BlockList0_6->removeAllObjects();
					for(nt=0;nt<(int)BlockList0_4->count();nt++){
						BlockList0_6->addObject(BlockList0_4->objectAtIndex(nt));
					}
				}
				//*/

				for(nt=0;nt<(int)BlockList0_4->count();nt++){
					BlockList0_5->addObject(BlockList0_4->objectAtIndex(nt));
				}
				
				BlockList0_4->removeAllObjects();
				countBlockTemp=0;
				BlockNumTemp=0;
			}
		}while(bools_1==0);

		//char numD[64];
		//sprintf(numD, "%d", BlockList0_3->count());
		//CCLog(numD);

		BlockScore+=((int)BlockList0_6->count())*((int)BlockList0_6->count())*5;
		LeftNum+=BlockList0_6->count();
		countBlock=0;
		BlockNums=0;

		for(yt=0;yt<(int)BlockList0_6->count();yt++){
			ListTemp=(CCSprite*)BlockList0_6->objectAtIndex(yt);
			for(xt=0;xt<(int)BlockList0->count();xt++){
				ListTemp1=(CCSprite*)BlockList0->objectAtIndex(xt);
				if(ListTemp->getTag()==ListTemp1->getTag()){
					if(ListTemp->getPosition().x==ListTemp1->getPosition().x&&ListTemp->getPosition().y==ListTemp1->getPosition().y){
						BlockList0_7->addObject(ListTemp1);
					}
				}
				
			}
		}
		for(yt=0;yt<(int)BlockList0_7->count();yt++){
			BlockList0->removeObject(BlockList0_7->objectAtIndex(yt));
		}
		BlockList0_7->removeAllObjects();
		BlockList0_6->removeAllObjects();

		BlockList0_1->removeAllObjects();
		BlockList0_4->removeAllObjects();

		BlockList1_1->removeAllObjects();
		BlockList2_1->removeAllObjects();
		BlockList3_1->removeAllObjects();
		BlockList4_1->removeAllObjects();
		BlockList5_1->removeAllObjects();
		BlockList6_1->removeAllObjects();
		BlockList7_1->removeAllObjects();
		BlockList8_1->removeAllObjects();
		BlockList9_1->removeAllObjects();
		BlockList10_1->removeAllObjects();

		for(xt=0;xt<(int)BlockList0->count();xt++){
			ListTemp1=(CCSprite*)BlockList0->objectAtIndex(xt);

			if(ListTemp1->getPosition().x<ListTemp1->getContentSize().width){
				BlockList1_1->addObject(ListTemp1);
			}else if(ListTemp1->getPosition().x<ListTemp1->getContentSize().width*2){
				BlockList2_1->addObject(ListTemp1);
			}else if(ListTemp1->getPosition().x<ListTemp1->getContentSize().width*3){
				BlockList3_1->addObject(ListTemp1);
			}else if(ListTemp1->getPosition().x<ListTemp1->getContentSize().width*4){
				BlockList4_1->addObject(ListTemp1);
			}else if(ListTemp1->getPosition().x<ListTemp1->getContentSize().width*5){
				BlockList5_1->addObject(ListTemp1);
			}else if(ListTemp1->getPosition().x<ListTemp1->getContentSize().width*6){
				BlockList6_1->addObject(ListTemp1);
			}else if(ListTemp1->getPosition().x<ListTemp1->getContentSize().width*7){
				BlockList7_1->addObject(ListTemp1);
			}else if(ListTemp1->getPosition().x<ListTemp1->getContentSize().width*8){
				BlockList8_1->addObject(ListTemp1);
			}else if(ListTemp1->getPosition().x<ListTemp1->getContentSize().width*9){
				BlockList9_1->addObject(ListTemp1);
			}else if(ListTemp1->getPosition().x<ListTemp1->getContentSize().width*10){
				BlockList10_1->addObject(ListTemp1);
			}else if(ListTemp1->getPosition().x<ListTemp1->getContentSize().width*11){
				BlockList11_1->addObject(ListTemp1);
			}else if(ListTemp1->getPosition().x<ListTemp1->getContentSize().width*12){
				BlockList12_1->addObject(ListTemp1);
			}else if(ListTemp1->getPosition().x<ListTemp1->getContentSize().width*13){
				BlockList13_1->addObject(ListTemp1);
			}
		}
		//整理13个数组
		if(BlockList1_1->count()>0){
			SortBlockList(BlockList1_1);
		}
		if(BlockList2_1->count()>0){
			SortBlockList(BlockList2_1);
		}else{
			MoveBlockList(BlockList1_1,1);
		}
		if(BlockList3_1->count()>0){
			SortBlockList(BlockList3_1);
		}else{
			MoveBlockList(BlockList2_1,1);
			MoveBlockList(BlockList1_1,1);
		}
		if(BlockList4_1->count()>0){
			SortBlockList(BlockList4_1);
		}else{
			MoveBlockList(BlockList3_1,1);
			MoveBlockList(BlockList2_1,1);
			MoveBlockList(BlockList1_1,1);
		}
		if(BlockList5_1->count()>0){
			SortBlockList(BlockList5_1);
		}else{
			MoveBlockList(BlockList4_1,1);
			MoveBlockList(BlockList3_1,1);
			MoveBlockList(BlockList2_1,1);
			MoveBlockList(BlockList1_1,1);
		}
		if(BlockList6_1->count()>0){
			SortBlockList(BlockList6_1);
		}else{
			MoveBlockList(BlockList5_1,1);
			MoveBlockList(BlockList4_1,1);
			MoveBlockList(BlockList3_1,1);
			MoveBlockList(BlockList2_1,1);
			MoveBlockList(BlockList1_1,1);
		}
		if(BlockList7_1->count()>0){
			SortBlockList(BlockList7_1);
		}else{
			MoveBlockList(BlockList6_1,1);
			MoveBlockList(BlockList5_1,1);
			MoveBlockList(BlockList4_1,1);
			MoveBlockList(BlockList3_1,1);
			MoveBlockList(BlockList2_1,1);
			MoveBlockList(BlockList1_1,1);
		}
		if(BlockList8_1->count()>0){
			SortBlockList(BlockList8_1);
		}else{
			MoveBlockList(BlockList9_1,-1);
			MoveBlockList(BlockList10_1,-1);
			MoveBlockList(BlockList11_1,-1);
			MoveBlockList(BlockList12_1,-1);
			MoveBlockList(BlockList13_1,-1);
		}
		if(BlockList9_1->count()>0){
			SortBlockList(BlockList9_1);
		}else{
			MoveBlockList(BlockList10_1,-1);
			MoveBlockList(BlockList11_1,-1);
			MoveBlockList(BlockList12_1,-1);
			MoveBlockList(BlockList13_1,-1);
		}
		if(BlockList10_1->count()>0){
			SortBlockList(BlockList10_1);
		}else{
			MoveBlockList(BlockList11_1,-1);
			MoveBlockList(BlockList12_1,-1);
			MoveBlockList(BlockList13_1,-1);
		}
		if(BlockList11_1->count()>0){
			SortBlockList(BlockList11_1);
		}else{
			MoveBlockList(BlockList12_1,-1);
			MoveBlockList(BlockList13_1,-1);
		}
		if(BlockList12_1->count()>0){
			SortBlockList(BlockList12_1);
		}else{
			MoveBlockList(BlockList13_1,-1);
		}
		if(BlockList13_1->count()>0){
			SortBlockList(BlockList13_1);
		}

		bools=1;
		//判定是否结束；
		CCSprite* tempSprite1,*tempSprite2,*tempSprite3;
		float tempSprite2X,tempSprite2Y,tempSprite3X,tempSprite3Y;
		int num=0;
		int num1=0;
		float lenth1=0;
		for(it=0;it<(int)BlockList0->count();it++){
			tempSprite1=(CCSprite*)BlockList0->objectAtIndex(it);
			BlockList0_4->addObject(tempSprite1);
			num=0;
			do{
				num=BlockList0_4->count();
				for(jt=0;jt<(int)BlockList0->count();jt++){
					tempSprite2=(CCSprite*)BlockList0->objectAtIndex(jt);
					if(BlockList0_4->containsObject(tempSprite2)){
						continue;
					}
					tempSprite2X = tempSprite2->getPosition().x;
					tempSprite2Y = tempSprite2->getPosition().y;
					num1=0;
					for(kt=0;kt<(int)BlockList0_4->count();kt++){
						tempSprite3=(CCSprite*)BlockList0_4->objectAtIndex(kt);
						tempSprite3X = tempSprite3->getPosition().x;
						tempSprite3Y = tempSprite3->getPosition().y;
						lenth1=0;
						if(tempSprite2->getTag()==tempSprite3->getTag()){
							if(tempSprite2X==tempSprite3X){
								lenth1 = tempSprite3Y-tempSprite2Y;
							}else if(tempSprite3Y==tempSprite2Y){
								lenth1 = tempSprite3X-tempSprite2X;
							}
							if(lenth1<0){
								lenth1=0-lenth1;
							}
							if(lenth1>0&&lenth1<=tempSprite3->getContentSize().width){
								num1=1;
								break;
							}
						}
					}
					if(num1==1){
						BlockList0_4->addObject(tempSprite2);	
					}
				}
			}while((int)BlockList0_4->count()>num);
			
			if(BlockList0_4->count()>2){
				bools=0;
				break;
			}
			BlockList0_4->removeAllObjects();
		}
	}while(bools==0);

	return BlockScore*10000+(LeftNum);
}
