#ifndef __HomePage_SCENE_H__
#define __HomePage_SCENE_H__

#include "cocos2d.h"

class HomePageScene:public cocos2d::CCScene{
public:
	bool init();
    void onPlay(CCObject* pSender);
    void onMore(CCObject* pSender);

	CREATE_FUNC(HomePageScene);

};

#endif